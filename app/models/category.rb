class Category < ActiveRecord::Base

  #gem friendly id
  include FriendlyId
  friendly_id :description, use: :slugged

  # associations
  has_many :ads

  #validations
  validates_presence_of :description

  #scopes
  scope :order_by_description, -> { order(:description) }

end
